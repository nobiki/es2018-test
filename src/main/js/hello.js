import { Sample } from 'Sample.js'
import { SuperSample } from 'SuperSample.js'

const foo = new Sample("aa", "bb")
foo.debug("cc")

const bar = new SuperSample("dd", "ee")
bar.debug("ff")
