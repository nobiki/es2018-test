var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');
var webpack = require('webpack');
// var jquery = require('jquery');

module.exports = {
  entry: {
    "main": path.join(__dirname, "./src/main/entry.js"),
  },
  output: {
    path: __dirname + '/public/',
    filename: "./js/[name].js"
  },
  resolve: {
    modules: ['./node_modules','./src/lib'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env',
              ]
            }
          }
        ],
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({fallback:'style-loader',use:'css-loader'})
      },
      {
        test: /\.(jpg|gif|png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      },
    ]
  },
  plugins: [
    // new webpack.ProvidePlugin({
    //     $: "jquery",
    //     jQuery: "jquery",
    //     "window.jQuery": "jquery"
    // }),
    new ExtractTextPlugin("./css/[name].css")
  ],
  performance: { hints: false },
  cache: true
};
